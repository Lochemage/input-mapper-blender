Input Mapper Module - by Jeff Houde (Lochemage)
  https://bitbucket.org/Lochemage/input-mapper-blender

This is an input mapper module for the Blender Game Engine that makes it easy to set up complex and dynamic user input controls.

####################################################################################
# Input Mapper module
#   Allow mapping of any keyboard, mouse, or joystick control
#
# Setup:
#   Import this module within your own and use the following
#   functions to setup your controls:
#
#     getMouseSensitivity()
#     setMouseSensitivity(value)
#       - Gets or Sets the sensitivity of mouse movement, default 0.1
#     getJoystickDeadzone(axisIndex, joyIndex = 0)
#     setJoystickDeadzone(value, axisIndex = -1, joyIndex = -1)
#       - Gets or Sets the 'deadzone' percentage of the joystick axis
#         which defines the amount of distance you have to move
#         it before a movement is registered.  The value must be
#         between 0 and 1, where 0 means no deadzone.  If axisIndex is
#         -1, or not supplied, then it sets all axis deadzones.
#         If joyIndex is -1 or not supplied, then all are effected.
#     update()
#       - Must be called at the beginning of every update, before
#         reading input status.  Specifically this will update
#         current mouse cursor movements as well as some button
#         statuses on joysticks.
#     pollKey(outputConsole = True)
#       - This function polls the keyboard for any user input and
#         returns the result.  Useful if you wish to map a key dynamically
#         by having the user press the key they wish to use.  By default,
#         this will also log a message in the console window, but this
#         can be turned off by passing in False for the 'outputConsole'
#         parameter.  The value returned is either blank or the string
#         value of the control activated.
#     pollMouse(outputConsole = True)
#       - Same as the pollKey, except with all mouse events.
#     pollJoy(outputConsole = True)
#       - Same as the pollKey, except with all joystick events.
#     poll(outputConsole = True)
#       - Performs pollKey(), pollMouse(), and pollJoy() together.
#     read(controls)
#       - Reads the status of a particular input control.  The control
#         parameter is a formatted string that determines exactly what
#         input to read.  The next section explains the control string
#         in more detail.  The returned result is always a float value
#         between 0 and 1, 0 being inactive.  Any value in between are
#         the result of an analog input such as a joystick.  The only
#         exception to this is the mouse movement, which can go beyond 1.
#
# Read Control String:
#   The control string is a formatted string of statements that determine
#   the input you are requesting.  If you want to allow alternate controls
#   that perform the same operation, such as walking forward with either
#   the 'w' key or the forward axis on the joystick, you can separate
#   multiple control statements by commas (',').  If you want to assign
#   a combination of controls that must be used together, such as
#   leftctrl and the 'c' key, then combine the two (or more) statements
#   with a plus ('+') sign.  You may combine as many statements as you
#   like in one formatted control string.  If an exclamation mark ('!')
#   is found directly before a statement, then the command will trigger
#   only if that input is not currently active.  More information on these
#   can be found at the bottom of this block.
#
#   Each statement consists of segments
#   separated by periods ('.') and always begins with the input device.
#   White spaces within the data string are ignored, so feel free to
#   use as many as you like.  If a statement is not recognized, a log
#   message will be printed stating so.
#   
#   The following are the format required for each device, formats
#   enclosed with brackets ('[]') are optional and can be omitted.
#
#     Keyboard format: keyboard.key.[state]
#       keyboard    - Can either be 'keyboard' or 'key'.
#       key         - The human readable name of the key ('w', or 'leftctrl').
#       [state]     - Optional state of the key, can be either 'active',
#                     'pressed', or 'released'.  If omitted, 'active' is assumed.
#
#     Mouse format: mouse.[type].control.[state]
#       mouse       - Must be 'mouse'.
#       [type]      - Optional type of input, can be either 'move', 'button',
#                     or 'btn'.  If omitted, 'move' is assumed.
#       control     - If type is 'move', then the control is the direction
#                     of the movement, 'up', 'down', 'left', 'right'.  If
#                     the type is 'button', then the control is the button
#                     which can be 'left', 'right', or 'middle'.
#       [state]     - If the control is a button, the state can be supplied
#                     to specify the state of that button, either 'active',
#                     'pressed', or 'released'.  If omitted, 'active' is assumed.
#
#     Joystick format: joystick.[joyIndex].[type].[hatIndex].[axisIndex].control.[state]
#       joystick    - Can either be 'joystick', 'joy', 'gamepad', or 'pad'.
#       [joyIndex]  - An optional index number to the joystick you are
#                     referencing.  If omitted, 0 is assumed.
#       [type]      - Optional type of input, can be either 'move', 'throttle',
#                     'twist', 'hat', 'axis', 'button', or 'btn'. If omitted,
#                     'move' is assumed.
#       [hatIndex]  - In the case that the type is 'hat', this can be supplied
#                     to specify the index number of the hat. If omitted, 0
#                     is assumed.
#       [axisIndex] - If type is 'axis', then this must indicate the index
#                     of the one desired.
#       control     - If type is 'move', 'throttle', 'twist', 'hat', or 'axis',
#                     then control is the direction of movement. It can be either
#                     'up', 'down', 'left', 'right', 'forward', 'back', 'backward',
#                     'upleft', 'upright', 'downleft', 'downright', 'neg',
#                     'negative', 'pos', 'positive', or 'full' if you want
#                     the entire range of an axis as a single 0-1 value.  If type
#                     is 'button' or 'btn', then the control can either be
#                     'trigger', 'secondary', or the index of the button desired.
#       [state]     - If control is a button or hat switch, the state can be
#                     supplied to specify the state of that button, either
#                     'active', 'pressed', or 'released'.  If omitted, 'active'
#                     is assumed.
#
# Data String Examples:
#
#   Keyboard:
#     Holding down the 'W' key.
#       key.w
#       key.w.active
#       keyboard.w.active
#     The moment the left control key is pressed:
#       key.leftctrl.pressed
#       keyboard.leftctrl.pressed
#
#   Mouse:
#     Moving the cursor to the left:
#       mouse.left
#       mouse.move.left
#     Holding down the middle mouse button:
#       mouse.button.middle
#       mouse.btn.middle
#       mouse.button.middle.active
#     The moment the left mouse button is released:
#       mouse.button.left.released
#       mouse.btn.left.released
#
#   Joystick:
#     Moving the joystick forward:
#       joy.forward
#       joystick.forward
#       pad.forward
#       gamepad.forward
#       joy.move.forward
#       joystick.axis.1.negative
#       pad.axis.1.neg
#       gamepad.axis.1.neg
#     Getting the absolute throttle position:
#       joy.throttle.full
#       joystick.axis.2.full
#     Holding down the trigger button:
#       pad.button.trigger
#       gamepad.btn.trigger
#       joy.button.0
#     The moment the secondary button is pressed:
#       joystick.button.secondary.pressed
#       pad.btn.1.pressed
#
#   Assigning multiple controls:
#     Pressing either the 'w' key, moving the joystick forward, or
#     moving your mouse up will all trigger a response from this:
#
#       joy.forward, key.w, mouse.up
#
#     The following will only trigger if the left mouse button is
#     pressed while you were already holding the left control key:
#
#       key.leftctrl+mouse.button.left.pressed
#
#     The following will either trigger by pressing both left shift and
#     'c', or it will trigger if you press the joystick button 1:
#
#       key.leftshift + key.c, joy.button.1
#
#     The following will only trigger if you are moving the mouse left
#     while NOT pressing the left mouse button:
#
#       mouse.left + !mouse.button.left
####################################################################################