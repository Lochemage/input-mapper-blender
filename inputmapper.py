####################################################################################
# Input Mapper module by Jeff Houde (Lochemage)
#   Allow mapping of any keyboard, mouse, or joystick control
#
# Setup:
#   Import this module within your own and use the following
#   functions to setup your controls:
#
#     getMouseSensitivity()
#     setMouseSensitivity(value)
#       - Gets or Sets the sensitivity of mouse movement, default 0.1
#     getJoystickDeadzone(axisIndex, joyIndex = 0)
#     setJoystickDeadzone(value, axisIndex = -1, joyIndex = -1)
#       - Gets or Sets the 'deadzone' percentage of the joystick axis
#         which defines the amount of distance you have to move
#         it before a movement is registered.  The value must be
#         between 0 and 1, where 0 means no deadzone.  If axisIndex is
#         -1, or not supplied, then it sets all axis deadzones.
#         If joyIndex is -1 or not supplied, then all are effected.
#     update()
#       - Must be called at the beginning of every update, before
#         reading input status.  Specifically this will update
#         current mouse cursor movements as well as some button
#         statuses on joysticks.
#     pollKey(outputConsole = True)
#       - This function polls the keyboard for any user input and
#         returns the result.  Useful if you wish to map a key dynamically
#         by having the user press the key they wish to use.  By default,
#         this will also log a message in the console window, but this
#         can be turned off by passing in False for the 'outputConsole'
#         parameter.  The value returned is either blank or the string
#         value of the control activated.
#     pollMouse(outputConsole = True)
#       - Same as the pollKey, except with all mouse events.
#     pollJoy(outputConsole = True)
#       - Same as the pollKey, except with all joystick events.
#     poll(outputConsole = True)
#       - Performs pollKey(), pollMouse(), and pollJoy() together.
#     read(controls)
#       - Reads the status of a particular input control.  The control
#         parameter is a formatted string that determines exactly what
#         input to read.  The next section explains the control string
#         in more detail.  The returned result is always a float value
#         between 0 and 1, 0 being inactive.  Any value in between are
#         the result of an analog input such as a joystick.  The only
#         exception to this is the mouse movement, which can go beyond 1.
#
# Read Control String:
#   The control string is a formatted string of statements that determine
#   the input you are requesting.  If you want to allow alternate controls
#   that perform the same operation, such as walking forward with either
#   the 'w' key or the forward axis on the joystick, you can separate
#   multiple control statements by commas (',').  If you want to assign
#   a combination of controls that must be used together, such as
#   leftctrl and the 'c' key, then combine the two (or more) statements
#   with a plus ('+') sign.  You may combine as many statements as you
#   like in one formatted control string.  If an exclamation mark ('!')
#   is found directly before a statement, then the command will trigger
#   only if that input is not currently active.  More information on these
#   can be found at the bottom of this block.
#
#   Each statement consists of segments
#   separated by periods ('.') and always begins with the input device.
#   White spaces within the data string are ignored, so feel free to
#   use as many as you like.  If a statement is not recognized, a log
#   message will be printed stating so.
#   
#   The following are the format required for each device, formats
#   enclosed with brackets ('[]') are optional and can be omitted.
#
#     Keyboard format: keyboard.key.[state]
#       keyboard    - Can either be 'keyboard' or 'key'.
#       key         - The human readable name of the key ('w', or 'leftctrl').
#       [state]     - Optional state of the key, can be either 'active',
#                     'pressed', or 'released'.  If omitted, 'active' is assumed.
#
#     Mouse format: mouse.[type].control.[state]
#       mouse       - Must be 'mouse'.
#       [type]      - Optional type of input, can be either 'move', 'button',
#                     or 'btn'.  If omitted, 'move' is assumed.
#       control     - If type is 'move', then the control is the direction
#                     of the movement, 'up', 'down', 'left', 'right'.  If
#                     the type is 'button', then the control is the button
#                     which can be 'left', 'right', or 'middle'.
#       [state]     - If the control is a button, the state can be supplied
#                     to specify the state of that button, either 'active',
#                     'pressed', or 'released'.  If omitted, 'active' is assumed.
#
#     Joystick format: joystick.[joyIndex].[type].[hatIndex].[axisIndex].control.[state]
#       joystick    - Can either be 'joystick', 'joy', 'gamepad', or 'pad'.
#       [joyIndex]  - An optional index number to the joystick you are
#                     referencing.  If omitted, 0 is assumed.
#       [type]      - Optional type of input, can be either 'move', 'throttle',
#                     'twist', 'hat', 'axis', 'button', or 'btn'. If omitted,
#                     'move' is assumed.
#       [hatIndex]  - In the case that the type is 'hat', this can be supplied
#                     to specify the index number of the hat. If omitted, 0
#                     is assumed.
#       [axisIndex] - If type is 'axis', then this must indicate the index
#                     of the one desired.
#       control     - If type is 'move', 'throttle', 'twist', 'hat', or 'axis',
#                     then control is the direction of movement. It can be either
#                     'up', 'down', 'left', 'right', 'forward', 'back', 'backward',
#                     'upleft', 'upright', 'downleft', 'downright', 'neg',
#                     'negative', 'pos', 'positive', or 'full' if you want
#                     the entire range of an axis as a single 0-1 value.  If type
#                     is 'button' or 'btn', then the control can either be
#                     'trigger', 'secondary', or the index of the button desired.
#       [state]     - If control is a button or hat switch, the state can be
#                     supplied to specify the state of that button, either
#                     'active', 'pressed', or 'released'.  If omitted, 'active'
#                     is assumed.
#
# Data String Examples:
#
#   Keyboard:
#     Holding down the 'W' key.
#       key.w
#       key.w.active
#       keyboard.w.active
#     The moment the left control key is pressed:
#       key.leftctrl.pressed
#       keyboard.leftctrl.pressed
#
#   Mouse:
#     Moving the cursor to the left:
#       mouse.left
#       mouse.move.left
#     Holding down the middle mouse button:
#       mouse.button.middle
#       mouse.btn.middle
#       mouse.button.middle.active
#     The moment the left mouse button is released:
#       mouse.button.left.released
#       mouse.btn.left.released
#
#   Joystick:
#     Moving the joystick forward:
#       joy.forward
#       joystick.forward
#       pad.forward
#       gamepad.forward
#       joy.move.forward
#       joystick.axis.1.negative
#       pad.axis.1.neg
#       gamepad.axis.1.neg
#     Getting the absolute throttle position:
#       joy.throttle.full
#       joystick.axis.2.full
#     Holding down the trigger button:
#       pad.button.trigger
#       gamepad.btn.trigger
#       joy.button.0
#     The moment the secondary button is pressed:
#       joystick.button.secondary.pressed
#       pad.btn.1.pressed
#
#   Assigning multiple controls:
#     Pressing either the 'w' key, moving the joystick forward, or
#     moving your mouse up will all trigger a response from this:
#
#       joy.forward, key.w, mouse.up
#
#     The following will only trigger if the left mouse button is
#     pressed while you were already holding the left control key:
#
#       key.leftctrl+mouse.button.left.pressed
#
#     The following will either trigger by pressing both left shift and
#     'c', or it will trigger if you press the joystick button 1:
#
#       key.leftshift + key.c, joy.button.1
#
#     The following will only trigger if you are moving the mouse left
#     while NOT pressing the left mouse button:
#
#       mouse.left + !mouse.button.left
####################################################################################

import math

####################################################################################
####################################################################################
# Keyboard object
####################################################################################
####################################################################################
class Keyboard:
  def __init__(self, keySens, logic, events):
    self.sensor = keySens
    self.logic = logic
    self.events = events
    if self.sensor:
      self.initCodes()

  def read(self, arg):
    result = -1
    # Format keyboard.key.[state]
    try:
      args = arg.split('.')
      args.pop(0)
      keyName = args.pop(0)
      state = 'active'
      if len(args):
        state = args.pop(0)

      if state == 'active':
        result = self.key(keyName)
      elif state == 'pressed':
        result = self.keyPressed(keyName)
      elif state == 'released':
        result = self.keyReleased(keyName)
    except ValueError:
      pass

    if result == -1:
      print('Keyboard control "' + arg + '" not recognized')
      result = 0

    return result

  def poll(self):
    for code in self.logic.keyboard.events:
      status = self.logic.keyboard.events[code]
      if status != self.logic.KX_INPUT_NONE:
        for eventName in self.codes:
          if code == self.codes[eventName]:
            if status == self.logic.KX_INPUT_ACTIVE:
              return 'keyboard.' + eventName + '.active'
            elif status == self.logic.KX_INPUT_JUST_ACTIVATED:
              return 'keyboard.' + eventName + '.pressed'
            elif status == self.logic.KX_INPUT_JUST_RELEASED:
              return 'keyboard.' + eventName + '.released'
            break

  def key(self, keyName):
    code = self.codeForKey(keyName);
    if code != None:
      status = self.logic.keyboard.events[code]
      if status == self.logic.KX_INPUT_ACTIVE or status == self.logic.KX_INPUT_JUST_ACTIVATED:
        return 1.0
    else:
      return -1
    return 0.0

  def keyPressed(self, keyName):
    code = self.codeForKey(keyName);
    if code != None:
      status = self.logic.keyboard.events[code]
      if status == self.logic.KX_INPUT_JUST_ACTIVATED:
        return 1.0
    else:
      return -1
    return 0.0

  def keyReleased(self, keyName):
    code = self.codeForKey(keyName);
    if code != None:
      status = self.logic.keyboard.events[code]
      if status == self.logic.KX_INPUT_JUST_RELEASED:
        return 1.0
    else:
      return -1
    return 0.0

  def codeForKey(self, keyName):
    code = keyName.lower()
    if code in self.codes:
      return self.codes[code]
    else:
      return None

  def initCodes(self):
    ####################################################################################
    # Below are the specific string values that are
    # recognized as valid keys(denoted by the values
    # found within the codes[] part), capitalization
    # does not matter.
    ####################################################################################

    self.codes = {}
    self.codes['a'] = self.events.AKEY
    self.codes['b'] = self.events.BKEY
    self.codes['c'] = self.events.CKEY
    self.codes['d'] = self.events.DKEY
    self.codes['e'] = self.events.EKEY
    self.codes['f'] = self.events.FKEY
    self.codes['g'] = self.events.GKEY
    self.codes['h'] = self.events.HKEY
    self.codes['i'] = self.events.IKEY
    self.codes['j'] = self.events.JKEY
    self.codes['k'] = self.events.KKEY
    self.codes['l'] = self.events.LKEY
    self.codes['m'] = self.events.MKEY
    self.codes['n'] = self.events.NKEY
    self.codes['o'] = self.events.OKEY
    self.codes['p'] = self.events.PKEY
    self.codes['q'] = self.events.QKEY
    self.codes['r'] = self.events.RKEY
    self.codes['s'] = self.events.SKEY
    self.codes['t'] = self.events.TKEY
    self.codes['u'] = self.events.UKEY
    self.codes['v'] = self.events.VKEY
    self.codes['w'] = self.events.WKEY
    self.codes['x'] = self.events.XKEY
    self.codes['y'] = self.events.YKEY
    self.codes['z'] = self.events.ZKEY

    self.codes['0'] = self.events.ZEROKEY
    self.codes['1'] = self.events.ONEKEY
    self.codes['2'] = self.events.TWOKEY
    self.codes['3'] = self.events.THREEKEY
    self.codes['4'] = self.events.FOURKEY
    self.codes['5'] = self.events.FIVEKEY
    self.codes['6'] = self.events.SIXKEY
    self.codes['7'] = self.events.SEVENKEY
    self.codes['8'] = self.events.EIGHTKEY
    self.codes['9'] = self.events.NINEKEY

    self.codes['capslock']   = self.events.CAPSLOCKKEY
    self.codes['leftctrl']   = self.events.LEFTCTRLKEY
    self.codes['rightctrl']  = self.events.RIGHTCTRLKEY
    self.codes['leftalt']    = self.events.LEFTALTKEY
    self.codes['rightalt']   = self.events.RIGHTALTKEY
    self.codes['leftshift']  = self.events.LEFTSHIFTKEY
    self.codes['rightshift'] = self.events.RIGHTSHIFTKEY

    self.codes['up']    = self.events.UPARROWKEY
    self.codes['down']  = self.events.DOWNARROWKEY
    self.codes['left']  = self.events.LEFTARROWKEY
    self.codes['right'] = self.events.RIGHTARROWKEY

    self.codes['pad0'] = self.events.PAD0
    self.codes['pad1'] = self.events.PAD1
    self.codes['pad2'] = self.events.PAD2
    self.codes['pad3'] = self.events.PAD3
    self.codes['pad4'] = self.events.PAD4
    self.codes['pad5'] = self.events.PAD5
    self.codes['pad6'] = self.events.PAD6
    self.codes['pad7'] = self.events.PAD7
    self.codes['pad8'] = self.events.PAD8
    self.codes['pad9'] = self.events.PAD9
    self.codes['pad.'] = self.events.PADPERIOD
    self.codes['pad/'] = self.events.PADSLASHKEY
    self.codes['pad*'] = self.events.PADASTERKEY
    self.codes['pad-'] = self.events.PADMINUS
    self.codes['pad+'] = self.events.PADPLUSKEY
    self.codes['padenter']  = self.events.PADENTER

    self.codes['f1'] = self.events.F1KEY
    self.codes['f2'] = self.events.F2KEY
    self.codes['f3'] = self.events.F3KEY
    self.codes['f4'] = self.events.F4KEY
    self.codes['f5'] = self.events.F5KEY
    self.codes['f6'] = self.events.F6KEY
    self.codes['f7'] = self.events.F7KEY
    self.codes['f8'] = self.events.F8KEY
    self.codes['f9'] = self.events.F9KEY
    self.codes['f10'] = self.events.F10KEY
    self.codes['f11'] = self.events.F11KEY
    self.codes['f12'] = self.events.F12KEY
    self.codes['f13'] = self.events.F13KEY
    self.codes['f14'] = self.events.F14KEY
    self.codes['f15'] = self.events.F15KEY
    self.codes['f16'] = self.events.F16KEY
    self.codes['f17'] = self.events.F17KEY
    self.codes['f18'] = self.events.F18KEY
    self.codes['f19'] = self.events.F19KEY

    self.codes['grave']         = self.events.ACCENTGRAVEKEY
    self.codes['backslash']     = self.events.BACKSLASHKEY
    self.codes['backspace']     = self.events.BACKSPACEKEY
    self.codes['comma']         = self.events.COMMAKEY
    self.codes['delete']        = self.events.DELKEY
    self.codes['end']           = self.events.ENDKEY
    self.codes['equal']         = self.events.EQUALKEY
    self.codes['escape']        = self.events.ESCKEY
    self.codes['home']          = self.events.HOMEKEY
    self.codes['insert']        = self.events.INSERTKEY
    self.codes['leftbracket']   = self.events.LEFTBRACKETKEY
    self.codes['return']        = self.events.LINEFEEDKEY
    self.codes['minus']         = self.events.MINUSKEY
    self.codes['pagedown']      = self.events.PAGEDOWNKEY
    self.codes['pageup']        = self.events.PAGEUPKEY
    self.codes['pause']         = self.events.PAUSEKEY
    self.codes['period']        = self.events.PERIODKEY
    self.codes["quote"]         = self.events.QUOTEKEY
    self.codes['rightbracket']  = self.events.RIGHTBRACKETKEY
    self.codes['enter']         = self.events.ENTERKEY
    self.codes['semicolon']     = self.events.SEMICOLONKEY
    self.codes['slash']         = self.events.SLASHKEY
    self.codes['space']         = self.events.SPACEKEY
    self.codes['tab']           = self.events.TABKEY


####################################################################################
####################################################################################
# Mouse object
####################################################################################
####################################################################################
class Mouse:
  def __init__(self, mouseSens, logic, render, events):
    self.sensor = mouseSens
    self.logic = logic
    self.render = render
    self.events = events

    self.init = False
    self.size = (render.getWindowWidth(), render.getWindowHeight())
    scrCenter = (self.size[0]//2, self.size[1]//2)
    self.center = (scrCenter[0]*(1.0/self.size[0]), scrCenter[1]*(1.0/self.size[1]))
    self.pos = logic.mouse.position

    self.sensitivity = 0.1

  def read(self, arg):
    result = -1
    # Format mouse.[type].control.[state]
    try:
      args = arg.split('.')
      args.pop(0)

      mouseType = args.pop(0)
      # Ignore optional argument if it is to 'move'
      if mouseType == 'move':
        mouseType = args.pop(0)

      if mouseType == 'up':
        result = self.axisPositive(1)
      elif mouseType == 'down':
        result = self.axisNegative(1)
      elif mouseType == 'left':
        result = self.axisPositive(0)
      elif mouseType == 'right':
        result = self.axisNegative(0)
      elif mouseType in ['button', 'btn']:
          # For button presses, get the button type
          buttonName = args.pop(0)
          buttonType = None

          if buttonName == 'left':
            buttonType = self.events.LEFTMOUSE
          elif buttonName == 'right':
            buttonType = self.events.RIGHTMOUSE
          elif buttonName == 'middle':
            buttonType = self.events.MIDDLEMOUSE

          if buttonType:
            # Get the optional button state
            state = 'active'
            if len(args):
              state = args.pop(0)

            if state == 'active':
              result = self.button(buttonType)
            elif state == 'pressed':
              result = self.buttonPressed(buttonType)
            elif state == 'released':
              result = self.buttonReleased(buttonType)
    except ValueError:
      pass

    if result == -1:
      print('Mouse control "' + arg + '" not recognized')
      result = 0

    return result

  def poll(self):
    if not self.sensor:
      return;

    if self.move[0] > 2.0:
      return 'mouse.move.left'
    elif self.move[0] < -2.0:
      return 'mouse.move.right'

    if self.move[1] > 2.0:
      return 'mouse.move.up'
    elif self.move[1] < -2.0:
      return 'mouse.move.down'

    button = None
    state = None

    # Does not support WHEELUPMOUSE and WHEELDOWNMOUSE events
    for button in [['middle', self.events.MIDDLEMOUSE], ['right', self.events.RIGHTMOUSE], ['left', self.events.LEFTMOUSE]]:
      status = self.sensor.getButtonStatus(button[1])
      if status == self.logic.KX_INPUT_ACTIVE:
        state = 'active'
      elif status == self.logic.KX_INPUT_JUST_ACTIVATED:
        state = 'pressed'
      elif status == self.logic.KX_INPUT_JUST_RELEASED:
        state = 'released'

      if status != self.logic.KX_INPUT_NONE:
        return 'mouse.button.' + button[0] + '.' + state
        break

  def setSensitivity(self, value):
    self.sensitivity = value

  def getSensitivity(self):
    return self.sensitivity

  def update(self):
    pos = self.logic.mouse.position
    move = [self.center[0] - pos[0], self.center[1] - pos[1]]
    
    xMove = int(self.size[0] * move[0])
    yMove = int(self.size[1] * move[1])
    
    if not self.init:
      self.init = True
      xMove = 0
      yMove = 0

    self.render.setMousePosition(self.size[0]//2, self.size[1]//2)
    self.move = (xMove, yMove)

  def axisPositive(self, axisIndex):
    if axisIndex < len(self.move) and axisIndex >= 0:
      move = self.move[axisIndex]
      if move > 0.0:
        return move * self.sensitivity
    else:
      return -1
    return 0.0

  def axisNegative(self, axisIndex):
    if axisIndex < len(self.move) and axisIndex >= 0:
      move = self.move[axisIndex]
      if move < 0.0:
        return -move * self.sensitivity
    else:
      return -1
    return 0.0

  def button(self, buttonIndex):
    if not self.sensor:
      return 0.0;

    status = self.sensor.getButtonStatus(buttonIndex)
    if status == self.logic.KX_INPUT_ACTIVE:
      return 1.0
    return 0.0

  def buttonPressed(self, buttonIndex):
    if not self.sensor:
      return 0.0;

    status = self.sensor.getButtonStatus(buttonIndex)
    if status == self.logic.KX_INPUT_JUST_ACTIVATED:
      return 1.0
    return 0.0

  def buttonReleased(self, buttonIndex):
    if not self.sensor:
      return 0.0;

    status = self.sensor.getButtonStatus(buttonIndex)
    if status == self.logic.KX_INPUT_JUST_RELEASED:
      return 1.0
    return 0.0


####################################################################################
####################################################################################
# Joystick object
####################################################################################
####################################################################################
class Joystick:
  def __init__(self, joySens, logic):
    self.sensors = []
    self.logic = logic

    self.deadzones = []

    self.buttonStates = []
    self.hatStates = []

    # We only care about joystick indices that exist.
    for joy in joySens:
      if joy.index < len(logic.joysticks) and logic.joysticks[joy.index]:
        joyIndex = len(self.sensors)
        self.sensors.append(joy)
        
        states = []
        for button in range(0, joy.numButtons):
          states.append(self.logic.KX_INPUT_NONE)
        self.buttonStates.append(states)

        hats = []
        for hatIndex in range(0, joy.numHats):
          hatStates = []
          for index in range(0, 12):
            hatStates.append(self.logic.KX_INPUT_NONE)
          hats.append(hatStates)
        self.hatStates.append(hats)

        axes = []
        for axisIndex in range(0, joy.numAxis):
          axes.append(0.2)
        self.deadzones.append(axes)

  def read(self, arg):
    result = -1
    # Format joystick.[joyIndex].[type].[hatIndex].[axisIndex].control.[state]
    try:
      args = arg.split('.')
      args.pop(0)

      joyType = args.pop(0)
      joyIndex = 0
      # Attempt to extract the joyIndex if supplied
      try:
        joyIndex = int(joyType)
        joyType = args.pop(0)
      except ValueError:
        pass

      # If the joyType is 'move', ignore it as cosmetic
      if joyType == 'move':
        joyType = args.pop(0)

      # Test each direction.
      if joyType == 'left':
        result = self.axisNegative(0, joyIndex)
      elif joyType == 'right':
        result = self.axisPositive(0, joyIndex)
      elif joyType in ['up', 'forward']:
        result = self.axisNegative(1, joyIndex)
      elif joyType in ['down', 'back', 'backward']:
        result = self.axisPositive(1, joyIndex)
      elif joyType == 'throttle':
        # Get the direction of the throttle control.
        direction = args.pop(0)

        if direction in ['up', 'forward', 'neg', 'negative']:
          result = self.axisNegative(2, joyIndex)
        elif direction in ['down', 'back', 'backward', 'pos', 'positive']:
          result = self.axisPositive(2, joyIndex)
        elif direction == 'full':
          result = 0.5 + (self.axisPositive(2, joyIndex) - self.axisNegative(2, joyIndex))

      elif joyType == 'twist':
        # Get the direction of the twist control.
        direction = args.pop(0)

        if direction in ['left', 'neg', 'negative']:
          result = self.axisNegative(3, joyIndex)
        elif direction in ['right', 'pos', 'positive']:
          result = self.axisPositive(3, joyIndex)
        elif direction == 'full':
          result = 0.5 + (self.axisPositive(3, joyIndex) - self.axisNegative(3, joyIndex))

      elif joyType == 'hat':
        # Get the direction of the hat switch
        direction = args.pop(0)

        # There may be multiple hat switches, check if this
        # argument is an index value for the hat. If it is,
        # then the next argument is actually the direction.
        hatIndex = 0
        try:
          hatIndex = int(direction)
          direction = args.pop(0)
        except ValueError:
          pass

        # Find the desired button state
        state = 'active'
        if len(args):
          state = args.pop(0)

        # Convert the direction to its equivalent index
        dirIndex = 0
        if direction == 'up':
          dirIndex = 1
        elif direction == 'right':
          dirIndex = 2
        elif direction == 'down':
          dirIndex = 4
        elif direction == 'left':
          dirIndex = 8
        elif direction in ['upright', 'rightup']:
          dirIndex = 3
        elif direction in ['downright', 'rightdown']:
          dirIndex = 6
        elif direction in ['downleft', 'leftdown']:
          dirIndex = 12
        elif direction in ['upleft', 'leftup']:
          dirIndex = 9

        if dirIndex > 0:
          if state == 'active':
            result = self.hat(dirIndex, hatIndex, joyIndex)
          elif state == 'pressed':
            result = self.hatPressed(dirIndex, hatIndex, joyIndex)
          elif state == 'released':
            result = self.hatReleased(dirIndex, hatIndex, joyIndex)

      elif joyType == 'axis':
        # If we are specifying a specific axis,
        # get that axis and test it.
        axisIndex = int(args.pop(0))

        # Now get the direction of the axis
        direction = args.pop(0)

        if direction in ['up', 'forward', 'left', 'neg', 'negative']:
          result = self.axisNegative(axisIndex, joyIndex)
        elif direction in ['down', 'back', 'backward', 'right', 'pos', 'positive']:
          result = self.axisPositive(axisIndex, joyIndex)
        elif direction == 'full':
          result = 0.5 + (self.axisPositive(axisIndex, joyIndex) - self.axisNegative(axisIndex, joyIndex))

      elif joyType in ['button', 'btn']:
        # If the movement is actually a button press
        buttonName = args.pop(0)
        buttonIndex = -1

        # Get the button index
        if buttonName == 'trigger':
          buttonIndex = 0
        elif buttonName == 'secondary':
          buttonIndex = 1
        else:
          # If the button is an index number, expect a number
          buttonIndex = int(buttonName)

        if buttonIndex > -1:
          # If there is another argument, it should
          # be requirements on how the button is pressed.
          state = 'active'
          if len(args):
            state = args.pop(0)

          if state == 'active':
            result = self.button(buttonIndex)
          elif state == 'pressed':
            result = self.buttonPressed(buttonIndex)
          elif state == 'released':
            result = self.buttonReleased(buttonIndex)
    except ValueError:
      pass

    if result == -1:
      print('Joystick control "' + arg + '" not recognized')
      result = 0

    return result

  def poll(self):
    for joyIndex in range(0, len(self.sensors)):
      sensor = self.sensors[joyIndex]
      for button in range(0, sensor.numButtons):
        status = self.buttonStatus(button, joyIndex)
        state = None
        if status == self.logic.KX_INPUT_ACTIVE:
          state = 'active'
        elif status == self.logic.KX_INPUT_JUST_ACTIVATED:
          state = 'pressed'
        elif status == self.logic.KX_INPUT_JUST_RELEASED:
          state = 'released'

        if state:
          return 'joystick.' + str(sensor.index) + '.button.' + str(button) + '.' + state

      for axisIndex in range(0, sensor.numAxis):
        val = sensor.axisValues[axisIndex] / 32768.0
        if val > 0.5:
          direction = None
          if axisIndex == 0:
            direction = 'move.right'
          elif axisIndex == 1:
            direction = 'move.backward'
          elif axisIndex == 2:
            direction = 'throttle.down'
          elif axisIndex == 3:
            direction = 'twist.right'
          else:
            direction = 'axis.' + str(axisIndex) + '.positive'

          return 'joystick.' + str(sensor.index) + '.' + direction

        elif val < -0.5:
          direction = None
          if axisIndex == 0:
            direction = 'move.left'
          elif axisIndex == 1:
            direction = 'move.forward'
          elif axisIndex == 2:
            direction = 'throttle.up'
          elif axisIndex == 3:
            direction = 'twist.left'
          else:
            direction = 'axis.' + str(axisIndex) + '.negative'

          return 'joystick.' + str(sensor.index) + '.' + direction

      dirNames = ['up', 'right', 'upright', 'down', '6', 'downright', '', 'left', 'upleft', '10', '11', 'downleft']
      for hatIndex in range(sensor.numHats):
        for dirIndex in range(0, 12):
          status = self.hatStatus(dirIndex + 1, hatIndex, joyIndex)

          if status == self.logic.KX_INPUT_ACTIVE:
            return 'joystick.' + str(sensor.index) + '.hat.' + str(hatIndex) + '.' + dirNames[dirIndex] + '.active'
          elif status == self.logic.KX_INPUT_JUST_ACTIVATED:
            return 'joystick.' + str(sensor.index) + '.hat.' + str(hatIndex) + '.' + dirNames[dirIndex] + '.pressed'
          elif status == self.logic.KX_INPUT_JUST_RELEASED:
            return 'joystick.' + str(sensor.index) + '.hat.' + str(hatIndex) + '.' + dirNames[dirIndex] + '.released'

  def getDeadzone(self, axisIndex, joyIndex):
    if joyIndex >= 0 and joyIndex < len(self.deadzones):
      if axisIndex >= 0 and axisIndex < len(self.deadzones[joyIndex]):
        return self.deadzones[joyIndex][axisIndex]
    return -1

  def setDeadzone(self, value, axisIndex, joyIndex):
    if joyIndex >= 0 and joyIndex < len(self.deadzones):
      if axisIndex >= 0 and axisIndex < len(self.deadzones[joyIndex]):
        self.deadzones[joyIndex][axisIndex] = value
      elif axisIndex == -1:
        for axisIndex in range(0, len(self.deadzones[joyIndex])):
          self.deadzones[joyIndex][axisIndex] = value
    elif joyIndex == -1:
      for joyIndex in range(0, len(self.deadzones)):
        self.setDeadzone(value, -1, joyIndex)

  def update(self):
    # Provide JUST_ACTIVATED and JUST_RELEASED status for buttons
    # that do not currently support it internally.
    for joyIndex in range(0, len(self.sensors)):
      sensor = self.sensors[joyIndex]
      for hatIndex in range(0, sensor.numHats):
        active = sensor.hatValues[hatIndex]
        for dirIndex in range(0, 12):
          status = self.hatStates[joyIndex][hatIndex][dirIndex]
          if dirIndex+1 == active:
            if status == self.logic.KX_INPUT_NONE:
              status = self.logic.KX_INPUT_JUST_ACTIVATED
            elif status == self.logic.KX_INPUT_JUST_ACTIVATED:
              status = self.logic.KX_INPUT_ACTIVE
            elif status == self.logic.KX_INPUT_JUST_RELEASED:
              status = self.logic.KX_INPUT_JUST_ACTIVATED
          else:
            if status == self.logic.KX_INPUT_ACTIVE:
              status = self.logic.KX_INPUT_JUST_RELEASED
            elif status == self.logic.KX_INPUT_JUST_ACTIVATED:
              status = self.logic.KX_INPUT_JUST_RELEASED
            elif status == self.logic.KX_INPUT_JUST_RELEASED:
              status = self.logic.KX_INPUT_NONE
          self.hatStates[joyIndex][hatIndex][dirIndex] = status

      for buttonIndex in range(0, sensor.numButtons):
        if sensor.getButtonStatus(buttonIndex) > 0:
          if self.buttonStates[joyIndex][buttonIndex] == self.logic.KX_INPUT_NONE:
            self.buttonStates[joyIndex][buttonIndex] = self.logic.KX_INPUT_JUST_ACTIVATED
          elif self.buttonStates[joyIndex][buttonIndex] == self.logic.KX_INPUT_JUST_ACTIVATED:
            self.buttonStates[joyIndex][buttonIndex] = self.logic.KX_INPUT_ACTIVE
        else:
          if self.buttonStates[joyIndex][buttonIndex] in [self.logic.KX_INPUT_JUST_ACTIVATED, self.logic.KX_INPUT_ACTIVE]:
            self.buttonStates[joyIndex][buttonIndex] = self.logic.KX_INPUT_JUST_RELEASED
          elif self.buttonStates[joyIndex][buttonIndex] == self.logic.KX_INPUT_JUST_RELEASED:
            self.buttonStates[joyIndex][buttonIndex] = self.logic.KX_INPUT_NONE

  def buttonStatus(self, buttonIndex, joyIndex = 0):
    if self.sensors and joyIndex >= 0 and joyIndex < len(self.sensors):
      if self.sensors[joyIndex].numButtons >= buttonIndex and buttonIndex >= 0:
        return self.buttonStates[joyIndex][buttonIndex];
      else:
        print("Joystick " + str(joyIndex) + " button " + str(button) + " not found")
    else:
      print("Joystick " + str(joyIndex) + " not found")
    return 0.0

  def button(self, buttonIndex, joyIndex = 0):
    if self.sensors and joyIndex >= 0 and joyIndex < len(self.sensors):
      if self.sensors[joyIndex].numButtons >= buttonIndex and buttonIndex >= 0:
        if self.buttonStatus(buttonIndex, joyIndex) == self.logic.KX_INPUT_ACTIVE:
          return 1.0
      else:
        print("Joystick " + str(joyIndex) + " button " + str(buttonIndex) + " not found")
    else:
      print("Joystick " + str(joyIndex) + " not found")
    return 0.0

  def buttonPressed(self, buttonIndex, joyIndex = 0):
    if self.sensors and joyIndex >= 0 and joyIndex < len(self.sensors):
      if self.sensors[joyIndex].numButtons >= buttonIndex and buttonIndex >= 0:
        if self.buttonStatus(buttonIndex, joyIndex) == self.logic.KX_INPUT_JUST_ACTIVATED:
          return 1.0
      else:
        print("Joystick " + str(joyIndex) + " button " + str(buttonIndex) + " not found")
    else:
      print("Joystick " + str(joyIndex) + " not found")
    return 0.0

  def buttonReleased(self, buttonIndex, joyIndex = 0):
    if self.sensors and joyIndex >= 0 and joyIndex < len(self.sensors):
      if self.sensors[joyIndex].numButtons >= buttonIndex and buttonIndex >= 0:
        if self.buttonStatus(buttonIndex, joyIndex) == self.logic.KX_INPUT_JUST_RELEASED:
          return 1.0
      else:
        print("Joystick " + str(joyIndex) + " button " + str(buttonIndex) + " not found")
    else:
      print("Joystick " + str(joyIndex) + " not found")
    return 0.0

  def hatStatus(self, dirIndex, hatIndex = 0, joyIndex = 0):
    if self.sensors and joyIndex >= 0 and joyIndex < len(self.sensors):
      if self.sensors[joyIndex].numHats >= hatIndex and hatIndex >= 0:
        return self.hatStates[joyIndex][hatIndex][dirIndex-1]
      else:
        print("Joystick " + str(joyIndex) + " hat " + str(hatIndex) + " not found")
    else:
      print("Joystick " + str(joyIndex) + " not found")
    return self.logic.KX_INPUT_NONE

  def hat(self, dirIndex, hatIndex = 0, joyIndex = 0):
    if self.sensors and joyIndex >= 0 and joyIndex < len(self.sensors):
      if self.sensors[joyIndex].numHats >= hatIndex and hatIndex >= 0:
        if self.hatStatus(dirIndex, hatIndex, joyIndex) == self.logic.KX_INPUT_ACTIVE:
          return 1.0
      else:
        print("Joystick " + str(joyIndex) + " hat " + str(hatIndex) + " not found")
    else:
      print("Joystick " + str(joyIndex) + " not found")
    return 0.0

  def hatPressed(self, dirIndex, hatIndex = 0, joyIndex = 0):
    if self.sensors and joyIndex >= 0 and joyIndex < len(self.sensors):
      if self.sensors[joyIndex].numHats >= hatIndex and hatIndex >= 0:
        if self.hatStatus(dirIndex, hatIndex, joyIndex) == self.logic.KX_INPUT_JUST_ACTIVATED:
          return 1.0
      else:
        print("Joystick " + str(joyIndex) + " hat " + str(hatIndex) + " not found")
    else:
      print("Joystick " + str(joyIndex) + " not found")
    return 0.0

  def hatReleased(self, dirIndex, hatIndex = 0, joyIndex = 0):
    if self.sensors and joyIndex >= 0 and joyIndex < len(self.sensors):
      if self.sensors[joyIndex].numHats >= hatIndex and hatIndex >= 0:
        if self.hatStatus(dirIndex, hatIndex, joyIndex) == self.logic.KX_INPUT_JUST_RELEASED:
          return 1.0
      else:
        print("Joystick " + str(joyIndex) + " hat " + str(hatIndex) + " not found")
    else:
      print("Joystick " + str(joyIndex) + " not found")
    return 0.0

  def axisPositive(self, axisIndex, joyIndex = 0):
    if self.sensors and joyIndex >= 0 and joyIndex < len(self.sensors):
      if self.sensors[joyIndex].numAxis >= axisIndex and axisIndex >= 0:
        val = self.sensors[joyIndex].axisValues[axisIndex] / 32768.0
        deadzone = self.getDeadzone(axisIndex, joyIndex)
        if val > deadzone:
          return (val - deadzone) / (1.0 - deadzone)
      else:
        print("Joystick " + str(joyIndex) + " axis " + str(button) + " not found")
    else:
      print("Joystick " + str(joyIndex) + " not found")
    return 0.0

  def axisNegative(self, axisIndex, joyIndex = 0):
    if self.sensors and joyIndex >= 0 and joyIndex < len(self.sensors):
      if self.sensors[joyIndex].numAxis >= axisIndex and axisIndex >= 0:
        val = self.sensors[joyIndex].axisValues[axisIndex] / 32768.0
        deadzone = self.getDeadzone(axisIndex, joyIndex)
        if val < -deadzone:
          return (-val - deadzone) / (1.0 - deadzone)
      else:
        print("Joystick " + str(joyIndex) + " axis " + str(button) + " not found")
    else:
      print("Joystick " + str(joyIndex) + " not found")
    return 0.0

###################################################
# Input Mapper object
class InputMapper:
  def __init__(self, logic, render, events):
    self.logic = logic
    self.render = render
    self.events = events

    cont = logic.getCurrentController()

    keySens = None
    mouseSens = None
    joySens = []

    for i in cont.sensors:
      senClass = str(i.__class__)
      if senClass == "<class 'SCA_KeyboardSensor'>":
        keySens = i
      elif senClass == "<class 'SCA_MouseSensor'>":
        mouseSens = i
      elif senClass == "<class 'SCA_JoystickSensor'>":
        joySens.append(i)

    self.keyboard = Keyboard(keySens, logic, events)
    self.mouse    = Mouse(mouseSens, logic, render, events)
    self.joystick = Joystick(joySens, logic)

  def update(self):
    self.mouse.update()
    self.joystick.update()

  def getMouseSensitivity(self):
    return self.mouse.getSensitivity()

  def setMouseSensitivity(self, value):
    self.mouse.setSensitivity(value)

  def getJoystickDeadzone(self, axisIndex, joyIndex = 0):
    return self.joystick.getDeadzone(axisIndex, joyIndex)

  def setJoystickDeadzone(self, value, axisIndex = -1, joyIndex = -1):
    self.joystick.setDeadzone(value, axisIndex, joyIndex)

  def pollKey(self, outputConsole = True):
    result = self.keyboard.poll()
    if outputConsole:
      print(result)
    return result

  def pollMouse(self, outputConsole = True):
    result = self.mouse.poll()
    if outputConsole:
      print(result)
    return result

  def pollJoy(self, outputConsole = True):
    result = self.joystick.poll()
    if outputConsole:
      print(result)
    return result

  def poll(self, outputConsole = True):
    return self.pollKey(outputConsole) or self.pollMouse(outputConsole) or self.pollJoy(outputConsole)

  def read(self, data):
    # Remove whitespace and split up each separate statement
    args = data.lower().replace(' ', '').split(',')
    finalResults = []
    for arg in args:
      subResults = []
      # Split combined statements
      subArgs = arg.split('+')
      for subArg in subArgs:

        # Check for 'not' operation
        notOperation = False
        if subArg.startswith('!'):
          notOperation = True
          subArg = subArg.replace('!', '')

        result = 0
        if subArg.startswith('keyboard.') or subArg.startswith('key.'):
          result = self.keyboard.read(subArg)
        elif subArg.startswith('mouse.'):
          result = self.mouse.read(subArg)
        elif subArg.startswith('joystick.') or subArg.startswith('joy.') or subArg.startswith('gamepad.') or subArg.startswith('pad.'):
          result = self.joystick.read(subArg)
        else:
          print('Input Mapper device "' + arg.split('.')[0] + '" not recognized')
          break

        # If we're using 'not' operation, then flip our result.
        if notOperation:
          if result == 0:
            # Set it to a very small number so that we do not receive a failure
            # for the set, while also attempting to preserve any analog results.
            result = 2.220446049250313e-16 # Using sys.float_info.epsilon value
          else:
            result = 0
        subResults.append(result)

      # If any statement in this combined set are inactive,
      # the entire set fails
      if 0 in subResults:
        finalResults.append(0)
      else:
        finalResults.append(max(subResults))

    return max(finalResults)

###################################################
# Public API
def create(logic, render, events):
  return InputMapper(logic, render, events)
