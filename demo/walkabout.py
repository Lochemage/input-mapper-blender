####################################################################################
# Walkabout movement script that utilizes
# the Input Mapper Module by Jeff Houde (Lochemage)
#   https://bitbucket.org/Lochemage/input-mapper-blender
#
# Setup:
#   Apply this script to the player object. This should
#   be a mesh with physics type 'Dynamic' and the scene
#   camera must be attached as a direct child of it. Ensure
#   that the following sensors exist on the object and are
#   connected to this script via Python script controller.
#     * Ray Sensor
#     * Keyboard Sensor
#     * Mouse Sensor
#     * Joystick Sensor
#     * Always Sensor
#
#   The following game properties will be used
#   if they are applied to the player object,
#   otherwise their default values will be applied:
#     * wa.height = Float with default 1.5
#         The height from the object's center to the ground.
#         If the height does not reach the ground, you will
#         never register as standing and therefore will not
#         be able to jump unless you are in fly mode.
#     * wa.walkSpeed = Float with default 10.0
#         The objects walk speed.
#     * wa.lookSpeed = Float with default 10.0
#         The objects look speed (includes turning).
#     * wa.jumpStrength = Float with default 10.0
#         The force applied to jump.
#     * wa.fly = Boolean with default False
#         True if the player can jump even in air
#     * wa.mouse.sensitivy = Float with default 0.2
#         Determines the sensitivity of the mouse.
#     * wa.joy.deadzone = Float with default 0.2
#         The threshold of joystick movement before movement is registered.
#
#   The following properties can be used to assign new input controls
#   for movement.  The values for each control is a special formatted string
#   that describes the device and a control from that device.  More
#   information on the string formatting can be found in the InputMapper.py
#   library included with this project.
#     * wa.control.forward = Walk forward
#         'W' key or Joystick forward
#     * wa.control.backward = Walk backward
#         'S' key or Joystick backward
#     * wa.control.strafeLeft = Walk to the left
#         'A' key or Joystick hat left
#     * wa.control.strafeRight = Walk to the right
#         'D' key or Joystick hat right
#     * wa.control.lookUp = Look up
#         'R' key or Mouse up or Joystick hat up
#     * wa.control.lookDown = Look down
#         'F' key or Mouse down or Joystick hat down
#     * wa.control.lookLeft = Turn to the left
#         'Q' key or Mouse left or Joystick left
#     * wa.control.lookRight = Turn to the right
#         'E' key or Mouse right or Joystick right
#     * wa.control.jump = Perform a jump action
#         'Space' key or Mouse button left or Joystick trigger
#     * wa.control.crouch = Slows your movement or holds you in air if in fly mode.
#         'C' key or 'Left Ctrl' key or Mouse button right or Joystick secondary
####################################################################################


from bge import logic, render, events
import math, sys

sys.path.append('..')

import inputmapper

####################################################################################
####################################################################################
# Walkabout object
####################################################################################
####################################################################################
class Walkabout:
  def __init__(self, cont):
    self.cont = cont
    self.owner = cont.owner

    alwaysSensor = None
    self.raySensor = None

    for i in self.cont.sensors:
      senClass = str(i.__class__)
      if senClass == "<class 'KX_RaySensor'>":
        self.raySens = i
      elif senClass == "<class 'SCA_AlwaysSensor'>":
        # Force pulse mode on the always sensor this script
        # receives continual updates.
        i.usePosPulseMode = True

    # Create an instance of the input mapper for this object.
    self.inputMapper = inputmapper.create(logic, render, events)

    # Retrieve our player properties.
    self.height       = self.getProp('wa.height', 1.5)
    self.walkSpeed    = self.getProp('wa.walkSpeed', 10.0)
    self.lookSpeed    = self.getProp('wa.lookSpeed', 10.0)
    self.jumpStrength = self.getProp('wa.jumpStrength', 10.0)
    self.fly          = self.getProp('wa.fly', False)

    # Retrieve our player control mappings
    self.forwardControl     = self.getProp('wa.control.forward',      'key.w,                                           joy.forward')
    self.backwardControl    = self.getProp('wa.control.backward',     'key.s,                                           joy.backward')
    self.strafeLeftControl  = self.getProp('wa.control.strafeLeft',   'key.a,                                           joy.hat.left')
    self.strafeRightControl = self.getProp('wa.control.strafeRight',  'key.d,                                           joy.hat.right')
    self.lookUpControl      = self.getProp('wa.control.lookUp',       'key.r,               mouse.up,                   joy.hat.up')
    self.lookDownControl    = self.getProp('wa.control.lookDown',     'key.f,               mouse.move.down,            joy.hat.down')
    self.lookLeftControl    = self.getProp('wa.control.lookLeft',     'key.q,               mouse.left,                 joy.left')
    self.lookRightControl   = self.getProp('wa.control.lookRight',    'key.e,               mouse.right,                joy.right')
    self.jumpControl        = self.getProp('wa.control.jump',         'key.space.pressed,   mouse.button.left.pressed,  joy.button.trigger.pressed')
    self.crouchControl      = self.getProp('wa.control.crouch',       'key.leftctrl, key.c, mouse.button.right,         joy.button.secondary')

    self.mouseSensitivity   = self.getProp('wa.mouse.sensitivity', 0.1)
    self.joyDeadzone        = self.getProp('wa.joy.deadzone', 0.2)

    # Assign the sensitivity of the mouse.
    self.inputMapper.setMouseSensitivity(self.mouseSensitivity)

    # Set deadzone on only x and y axes on all joysticks.
    self.inputMapper.setJoystickDeadzone(self.joyDeadzone, 0);
    self.inputMapper.setJoystickDeadzone(self.joyDeadzone, 1);

    # Force the ray sensor to aim down at the height of the player object.
    if self.raySens:
      self.raySens.axis = 5
      self.raySens.range = self.height

  def main(self):
    # Update the device mapper
    self.inputMapper.update()

    self.inputMapper.poll()

    forward     = self.inputMapper.read(self.forwardControl);
    backward    = self.inputMapper.read(self.backwardControl);
    strafeLeft  = self.inputMapper.read(self.strafeLeftControl);
    strafeRight = self.inputMapper.read(self.strafeRightControl);
    lookUp      = self.inputMapper.read(self.lookUpControl);
    lookDown    = self.inputMapper.read(self.lookDownControl);
    lookLeft    = self.inputMapper.read(self.lookLeftControl);
    lookRight   = self.inputMapper.read(self.lookRightControl);
    jump        = self.inputMapper.read(self.jumpControl);
    crouch      = self.inputMapper.read(self.crouchControl);

    scale = 1.0

    if crouch > 0.0:
      scale = 0.5

    if not self.fly and (not self.raySens or not self.raySens.positive):
      scale = 0.3

    self.owner.localLinearVelocity[0] = (strafeRight - strafeLeft) * self.walkSpeed * scale
    self.owner.localLinearVelocity[1] = (forward - backward) * self.walkSpeed * scale

    ori = self.owner.worldOrientation.to_euler()
    ori.z -= (lookRight - lookLeft) * self.lookSpeed / 500.0
    self.owner.worldOrientation = ori

    # Jump if on the ground or in fly mode.
    if jump > 0.0 and (self.fly or (self.raySens and self.raySens.positive)):
      self.owner.localLinearVelocity[2] = self.jumpStrength
    elif self.fly and crouch > 0.0:
      self.owner.localLinearVelocity[2] = 0.175

    camera  = self.owner.children[0];
    ori = camera.worldOrientation.to_euler()
    ori.x += (lookUp - lookDown) * self.lookSpeed / 500.0
    camera.worldOrientation = ori

  def combine(self, controls):
    if isinstance(controls, (int, float, complex)):
      return controls
    else:
      highest = 0
      for value in controls:
        if value > highest:
          highest = value
      return highest

  def getRaySensor(self):
    return self.raySens

  def getCollisionSensor(self):
    return self.colSens

  def getProp(self, propName, defVal):
    props = {}
    for i in self.owner.getPropertyNames():
      props[i.upper()] = i

    if propName.upper() in props:
      return self.owner[props[propName.upper()]]
    return defVal

  def setProp(self, propName, value):
    props = {}
    for i in self.owner.getPropertyNames():
      props[i.upper()] = i
    if propName.upper() in props:
      self.owner[props[propName.upper()]] = value
    else:
      self.own[propName] = value

def main():
  cont = logic.getCurrentController()
  owner = cont.owner

  if 'WALKABOUT' not in owner:
    owner['WALKABOUT'] = Walkabout(cont)

  owner['WALKABOUT'].main()


# Non-module execution mode (script)
if logic.getCurrentController().mode == 0:
  main()